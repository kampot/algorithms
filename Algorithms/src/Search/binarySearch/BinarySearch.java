package Search.binarySearch;

import java.util.Arrays;

public class BinarySearch {

    public static void main(String[] args) {
        int[] array = new int[]{1, 6, 43, 56, 8, 32, 57, 72, 30, 98};
        Arrays.sort(array);
        System.out.println(binarySearch(array, 32));
        System.out.println(recursiveBinarySearch(array, 0, array.length - 1, 32));
    }

    public static int binarySearch(int[] array, int elementToSearch) {
        int firstIndex = 0;
        int lastIndex = array.length - 1;

        // условие прекращения (Элемент не представлен)
        while (firstIndex <= lastIndex) {
            int middleIndex = (firstIndex + lastIndex) / 2;
            // если средний элемент - искомый, то возвращаем его
            if (array[middleIndex] == elementToSearch) {
                return middleIndex;
            }

            // если средний элемент меньше
            // направляем наш индекс в (middle + 1), убирая правую часть из рассмотрения
            else if (array[middleIndex] < elementToSearch) {
                firstIndex = middleIndex + 1;
            }

            // если средний элемент больше
            // направляем наш индекс в (middle - 1), убирая левую часть из рассмотрения
            else if (array[middleIndex] > elementToSearch) {
                lastIndex = middleIndex - 1;
            }
        }
        return -1;
    }

    public static int recursiveBinarySearch(int[] array, int firstElement, int lastElement, int elementToSearch) {
        if (lastElement >= firstElement) {
            int middle = firstElement + (lastElement - firstElement) / 2;

            // если средний элемент - искомый, вернуть его индекс
            if (array[middle] == elementToSearch) {
                return middle;
            }

            // если средний элемент больше искомого
            // вызываем метод рекрсивно по суженным данным правой части
            if (array[middle] > elementToSearch) {
                return recursiveBinarySearch(array, firstElement, middle - 1, elementToSearch);
            }

            // иначе вызываем метод рекурсивно с сужением левой части массива
            return recursiveBinarySearch(array, middle + 1, lastElement, elementToSearch);
        }
        return -1;
    }
}
