package Search.linearSearch;

public class LinearSearch {
    public static void main(String[] args) {
        int[] array = new int[]{89, 54, 32, 65, 76, 123, 7, 9, 46, 50};

        System.out.println(linearSearch(array, 7));
    }

    public static int linearSearch(int[] array, int elementToSearch) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == elementToSearch) {
                return i;
            }
        }
        return -1;
    }
}
