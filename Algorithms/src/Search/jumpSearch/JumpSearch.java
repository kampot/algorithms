package Search.jumpSearch;

import java.util.Arrays;

public class JumpSearch {
    public static void main(String[] args) {
        int[] array = new int[]{1, 6, 43, 56, 8, 32, 57, 72, 30, 98};
        Arrays.sort(array);
        System.out.println(jumpSearch(array, 32));
    }

    public static int jumpSearch(int[] array, int elementToSearch){
        int arrayLength = array.length;
        int jumpStep = (int) Math.sqrt(array.length);
        int previousStep = 0;

        while (array[Math.min(jumpStep, arrayLength) - 1] < elementToSearch) {
            previousStep = jumpStep;
            jumpStep += Math.sqrt(arrayLength);
            if (previousStep >= arrayLength) {
                return -1;
            }
        }

        while (array[previousStep] < elementToSearch) {
            previousStep ++;
            if (previousStep == Math.min(jumpStep, arrayLength)) {
                return -1;
            }
        }

        if (array[previousStep] == elementToSearch) {
            return previousStep;
        }
        return -1;
    }

}
