package Sort.bubbleSort;

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {
        int[] array = new int[]{1, 6, 43, 56, 8, 32, 57, 72, 30, 98};
        bubbleSort(array);
        System.out.println(Arrays.toString(array));

        int[][] sortMatrix = new int[][]{
                {8, 3, 5},
                {1, 4, 6},
                {9, 7, 2}
        };

        matrixBubbleSort(sortMatrix);

        for (int i = 0; i < sortMatrix.length; i++) {
            for (int j = 0; j < sortMatrix[i].length; j++) {
                System.out.print(sortMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void bubbleSort(int[] array){
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j + 1] < array[j]) {
                    int swap = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = swap;
                }
            }
        }
    }

    public static int[][] matrixBubbleSort(int[][] sortMatrix){
        int swap;
        for (int i = 0; i < sortMatrix.length; i++) {
            for (int j = 0; j < sortMatrix[i].length; j++) {
                for (int k = 0; k < sortMatrix.length; k++) {
                    for (int l = 0; l < sortMatrix[k].length; l++) {
                        if (sortMatrix[i][j] <= sortMatrix[k][l]) {
                            swap = sortMatrix[i][j];
                            sortMatrix[i][j] = sortMatrix[k][l];
                            sortMatrix[k][l] = swap;
                        }
                    }
                }
            }
        }
        return sortMatrix;
    }
}
