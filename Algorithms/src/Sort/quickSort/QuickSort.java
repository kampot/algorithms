package Sort.quickSort;

import java.util.Arrays;

public class QuickSort {
    public static void main(String[] args) {
        int[] array = {12, 6, 4, 1, 15, 10};
        quickSort(array, 0, array.length - 1);
        System.out.println(Arrays.toString(array));
    }

    public static void quickSort(int[] array, int low, int high){
        // завершить, если массив пуст или уже нечего делить
        if (array.length == 0 || low >= high) {
            return;
        }

        // выбираем опорный элемент
        int middle = low + (high - low) / 2;
        int border = array[middle];

        // разделяем на подмассивы и меняем местами
        int i = low, j = high;
        while (i <= j) {
            while (array[i] < border) i++;
            while (array[j] > border) j--;
            if (i <= j) {
                int swap = array[i];
                array[i] = array[j];
                array[j] = swap;
                i++;
                j--;
            }
        }
        if (low < j) quickSort(array, low, j);
        if (high > i) quickSort(array, i, high);
    }
}
